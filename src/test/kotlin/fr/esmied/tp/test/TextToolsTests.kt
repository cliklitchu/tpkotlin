package fr.esmied.tp.test

import fr.esimed.tp.levenshteinDistance
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class TextToolsTests {

    @Test
    fun levenshteinDistanceTest(){
        Assertions.assertAll(
            org.junit.jupiter.api.function.Executable { Assertions.assertEquals(6, levenshteinDistance("Arnaud", "Julien")) },
            org.junit.jupiter.api.function.Executable { Assertions.assertEquals(6, levenshteinDistance("Arnaud", "Julien")) },
            org.junit.jupiter.api.function.Executable { Assertions.assertEquals(1, levenshteinDistance("Julie", "Julien")) } )
    }
}